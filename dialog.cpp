#include <QThread>
#include "operate_on_bulb.h"
#include "dialog.h"
#include "ui_dialog.h"
#include <QString>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    ui->bulb_ip_lineEdit->setText("192.168.1.139");
    ui->bulb_port_lineEdit->setText("55443");
    ui->brightness_horizontalSlider->setValue(50);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_toggle_bulb_pushButton_clicked()
{
    operate_on_bulb operate_on_bulb_obj;
    operate_on_bulb_obj.toggle_bulb(ui->bulb_ip_lineEdit->text(),ui->bulb_port_lineEdit->text().toUShort());

}

void Dialog::on_start_color_flow_pushButton_clicked()
{
    operate_on_bulb operate_on_bulb_obj;
    operate_on_bulb_obj.start_cf(ui->bulb_ip_lineEdit->text(),ui->bulb_port_lineEdit->text().toUShort());
}

void Dialog::on_set_ct_pushButton_clicked()
{
    operate_on_bulb operate_on_bulb_obj;
    operate_on_bulb_obj.set_ct(ui->bulb_ip_lineEdit->text(),ui->bulb_port_lineEdit->text().toUShort());
}

void Dialog::on_brightness_horizontalSlider_sliderMoved(int position)
{

}
